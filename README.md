# Face tracking with custom images, from an anime example and base implementation

# Installation
```
conda create -n env_name python=2.7
activate env_name
pip install opencv-python
pip install pygame
```

### TODO : Better close-up recognition instead of counting pixels in camera image